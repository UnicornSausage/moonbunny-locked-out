// room script file

function room_FirstLoad()
{
  aTheme_song.Play(eAudioPriorityHigh);
  player.Walk(86, 185, eBlock, eAnywhere);
  player.FaceDirection(eDirectionDown);
  player.Say("Whew!");
  player.Say("That was a long walk home from the bar.");
  player.FaceDirection(eDirectionLeft);
  player.Say("I'm ready to curl up and watch TV.");
  player.FaceDirection(eDirectionUp);
  player.FaceDirection(eDirectionDown);
  player.FaceDirection(eDirectionUp);
  player.Say("Uh oh.");
  Wait(20);
  player.FaceDirection(eDirectionDown);
  player.Say("I think I dropped my keys somewhere...");
  player.Say("I guess I should find them.");
}
function hDoor_Interact()
{
  if (player.HasInventory(iKey)) {
    cBun.Say("It's unlocked!");
  }
  else {
    cBun.Say("Nah, it's locked.");
  }
}
